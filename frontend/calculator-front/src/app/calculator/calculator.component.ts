import { Component, OnDestroy, OnInit, forwardRef } from '@angular/core';
import { Subject } from 'rxjs';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { CalculatorModel } from '../models/api-calculator-model';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting:CalculatorComponent
    }]
})

export class CalculatorComponent implements ControlValueAccessor,OnInit, OnDestroy {

  value = 10;
  calculatorResult: CalculatorModel;
  private propagateTouch :  () => void;
  private propagateChange : (value:number) => void;
  isMin=false;
  isMax=false;
  private ngUnsubcribe = new Subject();

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.propagateTouch();
  }

  ngOnDestroy(): void {
    this.ngUnsubcribe.next();
    this.ngUnsubcribe.complete();
    this.ngUnsubcribe.unsubscribe();
  }

  setFloorOrCeil(value: number): void {
    this.value = value;
    this.onCalculatorClick();
  }
  previousOrNextValue(sign: string): void {
    let nextValue = 0;
    switch (sign) {
      case 'plus':
        nextValue = this.value + 1;
        break;

      case 'min':
        nextValue = this.value - 1;
        break;
    }

    this.apiService
      .getCards(5, nextValue)
      .pipe(takeUntil(this.ngUnsubcribe))
      .subscribe((result: CalculatorModel) => {
        if (result) {
          //check if it's the minimum or maximum value possible
          this.isMin = result.ceil && (!result.floor);
          this.isMax = result.floor && !result.ceil;
          switch (sign) {
            case 'plus':
              if (result.ceil) {
                this.setFloorOrCeil(result.ceil.value);
              }
              break;
            case 'min':
              if (result.floor) {
                this.setFloorOrCeil(result.floor.value);
              }
              break;
          }
        }
      });
  }

  onCalculatorClick(): void {
    this.apiService
      .getCards(5, this.value)
      .pipe(takeUntil(this.ngUnsubcribe))
      .subscribe((result: CalculatorModel) => {

        //CASE 1 : value > possible value
        if(result.floor && result.floor.value < this.value && !result.ceil && !result.equal){
          this.setFloorOrCeil(result.floor.value);
        }

        //CASE 2 : value < possible value
        else if(result.ceil && result.ceil.value > this.value && !result.floor && !result.equal){
          this.setFloorOrCeil(result.ceil.value);
        }
        else{

        //CASE 3 : value != desired value
          this.calculatorResult = result;

        //CASE 4 : value = desired value
          if(this.calculatorResult.equal){
            this.propagateChange(this.value);
          }
        }
      }, (error: any) => {
        console.error(error);
      });
  }

  writeValue(value: number): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }


}
