import { CalculatorComponentValue } from './calculator-component-value';

export interface CalculatorModel {
  equal: CalculatorComponentValue;
  floor: CalculatorComponentValue;
  ceil: CalculatorComponentValue;
}
