import { Injectable } from '@angular/core';
import { CalculatorModel } from '../models/api-calculator-model';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) {}

  getCards(id: number, amount: number): Observable<CalculatorModel> {
    return this.httpClient.get<CalculatorModel>(`${environment.baseURL}/shop/${id}/search-combination?amount=${amount}`);
  }
}
