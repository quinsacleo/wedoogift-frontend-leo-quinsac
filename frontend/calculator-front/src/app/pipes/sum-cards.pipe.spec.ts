import { SumCardsPipe } from './sum-cards.pipe';

describe('SumCardsPipe', () => {
  it('create an instance', () => {
    const pipe = new SumCardsPipe();
    expect(pipe).toBeTruthy();
  });

  it('should sum correctly', () => {
    const pipe = new SumCardsPipe();
    expect(pipe.transform([3,10, 100, 1000])).toEqual('1113 € (3€ + 10€ + 100€ + 1000€)');
  });
});
