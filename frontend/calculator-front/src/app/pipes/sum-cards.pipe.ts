import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sumCards'
})
export class SumCardsPipe implements PipeTransform {

  transform(CardsValues: number[]): string {
    const total: number = CardsValues.reduce((accumulator: number, currentValue: number) => accumulator + currentValue, 0);
    const valueStr = CardsValues.join('€ + ');
    return (total+'€ ('+valueStr+'€)');
  }

}
